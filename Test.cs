﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculators;

namespace CalculatorsTest
{
	[TestFixture]
	public class TestCalculator
	{
		[Test]
		public void TestAdditional()
		{
			Calculator MyCalc = new Calculator();

			Assert.AreEqual(2, MyCalc.Additional(-3, 5), "-3+5 should be equal to 2");
		}
		[Test]
		public void TestSubstruction()
		{
			Calculator MyCalc = new Calculator();
			Assert.AreEqual(-0.5, MyCalc.Substruction(4.5, 5), "4,5-5 should be equal to -0,5");
		}
		[Test]
		public void TestDivision()
		{
			Calculator MyCalc = new Calculator();

			Assert.AreEqual(-3, MyCalc.Division(6, -2), "6/-2 should be equal to -3");
		}
		[Test]
		public void TestMultiplication()
		{
			Calculator MyCalc = new Calculator();

			Assert.AreEqual(12, MyCalc.Multiplication(3, 4), "3*4 should be equal to 12");
		}
	}
		[TestFixture]
		public class TestCalculatorIng
		{
			[Test]
		public void TestSquare()
		{
			CalculatorIng MyCalc = new CalculatorIng();

			Assert.AreEqual(12, MyCalc.Square(144), "Square root of 144 should be equal to 12");
		}

		[Test]
		public void TestDegree()
		{
			CalculatorIng MyCalc = new CalculatorIng();
			Assert.AreEqual(8, MyCalc.Degree(2, 3), "Degree 2^3 should be equal to 8");
		}
		[Test]
		public void TestAdditionalIng()
		{
			CalculatorIng MyCalc = new CalculatorIng();

			Assert.AreEqual(7.1, MyCalc.Additional(7, 0.1), "7+0.1 should be equal to 7.1");
		}
		[Test]
		public void TestSubstructionIng()
		{
			Calculator MyCalc = new Calculator();
			Assert.AreEqual(8, MyCalc.Substruction(6, -2), "6-(-2) should be equal to 8");
		}
		[Test]
		public void TestDivisionIng()
		{
			Calculator MyCalc = new Calculator();

			Assert.AreEqual(2.5, MyCalc.Division(5, 2), "5/2 should be equal to 2.5");
		}
		[Test]
		public void TestMultiplicationIng()
		{
			Calculator MyCalc = new Calculator();

			Assert.AreEqual(-7, MyCalc.Multiplication(7, -1), "7 *-1 should be equal to -7");
		}
	}
}

	
